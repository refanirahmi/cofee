<script type="text/javascript">
function showConfirm(id)
{
	var c = confirm("Are you sure wish to delete this item");

	if(c)
		window.location = "coffeeoverview.php?delete=" + id;
}

</script>

<?php
require ("coffeemode.php");

class coffeecontroler
{
	function CreateOverviewTable()
	{
		$result = "
		<table class='overviewTable'>
		<tr>
			<td></td>
			<td></td>
			<td><b>Id</b></td>
			<td><b>Name</b></td>
			<td><b>Type</b></td>
			<td><b>Price</b></td>
			<td><b>Roast</b></td>
			<td><b>Country</b></td>
			</tr>";

	
		$coffeeArray = $this->GetCoffeeByType('%');

		foreach ($coffeeArray as $key => $value)
		{
			$result = $result .
			"<tr>
				<td><a href='coffeeadd.php?Update=$value->id'>Update</a></td>
				<td><a href='#' onclick='showConfirm($value->id'>Delete</a></td>
				<td>$value->id</td>
				<td>$value->name</td>
				<td>$value->type</td>
				<td>$value->price</td>
				<td>$value->roast</td>
				<td>$value->country</td>

			</tr>";
		}

		$result = $result . "</table>";
	}


	function CreatCoffeeDropdownList()
	{
		$coffeemode = new coffeemode();
		$result = "<form action = '' method = 'post' width = '200px'>
		Please select a type;
		<select name = 'types' >
		    <option value = '%' >All</option>
		    ".$this->CreateOptionValues($coffeemode->GetCoffeeTypes(().
		    "<select>
		    <input type = 'submit' value = 'search' />
		    </form>";

		return result;
	}

	function CreateOptionValues(array $valuearray )
	{
		$result = "";

		foreach ($valuearray as $value)
		{
			$result = $result . "<option value='$value'>$value</option>";
		}

		return $result;
	}

	function CreateCoffeeTables($types)
	{
		$coffeemode = new coffeemode();
		$coffeeArray = $coffeemode->GetCoffeeTypes($types);
		result = "";

		foreach ($coffeeArray as $key =>$coffee)
		{
			$result = $result .
						"<table class = 'coffeeTable'>
						<tr>
							<th> rowspan='6' width= '150px' > <img runat = 'server' src = '$coffee->image' /></th>
							<th width = '75px' >Name: </th>
							<td>$coffee->name</td>
						</tr>

						<tr>
							
							<th>TYpe: </th>
							<td>$coffee->type</td>
						</tr>

						<tr>
							
							<th>Price: </th>
							<td>$coffee->price</td>
						</tr>

						<tr>
							
							<th>Roast: </th>
							<td>$coffee->roast</td>
						</tr>

						<tr>
							
							<th>Origin: </th>
							<td>$coffee->country</td>
						</tr>

						<tr>
							<td colspan='2' >$coffee->review</td>
						</tr>

						</table>";
		}

	}

	function GetImage()
	{
		$handle = opendir ("image");

		while ($image = readdir ($handle)){
			$image[] = $image;
		}
		closedir($handle);

		$imageArray = array();
		foreach($image as $image)
		{
			if(strlen($image) > 2)
			{
				array_push($imageArray, $image);
			}
		}

		$result = $this->CreateOptionValues($imageArray);
		return $result;
	}

	function InsertCoffee() 
	{
		$name = $_POST["textName"];
		$type = $_POST["ddltype"];
		$price = $_POST["textPrice"];
		$roast = $_POST["textRoast"];
		$country = $_POST["textCountry"];
		$image = $_POST["textImage"];
		$review = $_POST["textreview"];

		$coffee = new coffeeEntity((-1,$name,$type,$price,$roast,$country,$image,$review);
			$coffeemode = new coffeemode();
			$coffeemode->InsertCoffee($coffee);
	}

	function UpdateCoffee($id) 
	{
		$name = $_POST["textName"];
		$type = $_POST["ddltype"];
		$price = $_POST["textPrice"];
		$roast = $_POST["textRoast"];
		$country = $_POST["textCountry"];
		$image = $_POST["textImage"];
		$review = $_POST["textreview"];

		$coffee = new coffeeEntity(($id,$name,$type,$price,$roast,$country,$image,$review);
		$coffeemode = new coffeemode();
		$coffeemode->UpdateCoffee($id, $coffee);
	}

	function DeleteCoffee($id) 
	{
		$coffeemode = new coffeemode();
		$coffeemode->DeleteCoffee($id);
	}
	

	function GetCoffeeById($id) 
	{
		$coffeemode = new coffeemode();
		return $coffeemode->GetCoffeeById($id);
	}

	function GetCoffeeType($type) 
	{
		$coffeemode = new coffeemode();
		return $coffeemode->GetCoffeeByType($type);
	}

	function GetCoffeeTypes() 
	{
		$coffeemode = new coffeemode();
		return $coffeemode->GetCoffeeByTypes($types);
	}
}

?>