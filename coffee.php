<?php
$title = "Home";
$content = '<img src="coffee1.jpg" class="imgLeft" />

<h3>Title 1</h3>
<p>
	Jika kedai kopi adalah sebuah ‘ranah’ yang seharusnya meresap sebuah budaya tempat dia berada, maka Filosofi Kopi Jogja adalah salah satu kedai yang berhasil mewujudkannya.
</p>

<img src="coffee2.jpg" class="imgRight" />
<h3>Title 1</h3>
<p>
	Filosofi Kopi Jogja terdiri dari beberapa bagian. Ada bagian luar yaitu di taman dengan lampu-lampu dan kursi meja santai, ada bagian coffee bar yang di depannya tersedia lesehan, ada beberapa ruangan semi terbuka yang adem untuk duduk sambil ngopi santai. Semua ruangan tanpa mesin pendingin. Terbuka dengan pendingin alami yang sejuk nan asri. Sangkin asri dan damainya saya masih bisa mendengar suara jangkring sayup-sayup dari kejauhan. Persis seperti ngopi di desa.
</p>

<img src="coffee3.jpg" class="imgLeft" />
<h3>Title 1</h3>
<p>
	Saat ke sini saya memerhatikan segalanya yang sempat ditampilkan dalam film. Logo ‘cangkir filosofi’ yang ikonik itu terpampang pada dinding kayu. Seperti menyambut para pendatang dengan senyumnya yang khas. Yang tak kalah unik dari tempat ini adalah lesehan yang berada di depan bar. Bangunan yang hampir mirip rumah adat Jogja Bangsal Kencono ternyata bisa dijadikan kedai kopi yang berciri.
</p>

