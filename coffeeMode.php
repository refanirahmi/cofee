<?php
require ("coffeeEntity.php");
class coffeeMode
{
	function GetCoffeeType()
	{
		require 'credentials.php';
		mysql_connect($host, $user, $passwd) or die(mysql_error());
		mysql_select_db($database);
		$result = mysql_query("SELECT DISTINCT type FROM coffee") or die(mysql_error());
		$types = array();

		while($row = mysql_fetch_array($result)) 
		{
			array_push ($types, $row[0]);
		}

		mysql_close();
		return $types;
	}

	function GetCoffeeByType($type)
	{
		require 'credentials.php';

		mysql_connect($host, $user, $passwd) or die(mysql_err);
		mysql_select_db($database);
		$query = "SELECT * FROM coffee WHERE type LIKE '$type";
		$result = mysql_query($query) or die (mysql_error());
		$coffeeArray = array();

		while ($row = mysql_fetch_array($result))
		{
			$id = $row[0];
			$name =$row[1];
			$type = $row[2];
			$price = $row[3];
			$roast = $row[4];
			$country = $row[5];
			$image = $row[6];
			$review = $row[7];

			$coffee = new coffeeEntity($id,$name,$type,$price,$roast,$country,$image,$review);
			array_push($coffeeArray, $coffee);

		}

		mysql_close();
		return $coffeeArray;
	}

	function GetCoffeeById($id)
	{
		require 'credentials.php';

		mysql_connect($host, $user, $passwd) or die(mysql_err);
		mysql_select_db($database);
		$query = "SELECT * FROM coffee WHERE id = $id";
		$result = mysql_query($query) or die (mysql_error());
		

		while ($row = mysql_fetch_array($result))
		{
			$name =$row[1];
			$type = $row[2];
			$price = $row[3];
			$roast = $row[4];
			$country = $row[5];
			$image = $row[6];
			$review = $row[7];

			$coffee = new coffeeEntity($id,$name,$type,$price,$roast,$country,$image,$review);
			

		}

		mysql_close();
		return $coffee;
	}

	function insertCoffee(coffeeEntity $coffee)
	{
		
		$query = sprintf("INSERT INTO coffee
			            (name, type, price, roast, country, image, review)
			             VALUE
			             ('%s', '%s', '%s', '%s', '%s', '%s', '%s',)",
		mysql_real_escape_string($coffee->name),
		mysql_real_escape_string($coffee->type),
		mysql_real_escape_string($coffee->price),
		mysql_real_escape_string($coffee->roast),
		mysql_real_escape_string($coffee->country),
		mysql_real_escape_string($coffee->"./image" . $coffee->image),
		mysql_real_escape_string($coffee->review);

		$this->PerformQuery($query);
	}

	function UpdateCoffee($id, coffeeEntity $coffee)
	{
		$query = ("UPDATE coffee
					SET name = '%s', type = '%s', price = '%s', roast = '%s', country = '%s', image = '%s', review = '%s',
					WHERE id = $id",
		mysql_real_escape_string($coffee->name),
		mysql_real_escape_string($coffee->type),
		mysql_real_escape_string($coffee->price),
		mysql_real_escape_string($coffee->roast),
		mysql_real_escape_string($coffee->country),
		mysql_real_escape_string($coffee->"./image" . $coffee->image),
		mysql_real_escape_string($coffee->review);

		$this->PerformQuery($query));
	}

	function DeleteCoffee($id)
	{
		$query = "DELETE FROM coffee WHERE id = $id";
		$this->PerformQuery($query);
	}

	function PerformQuery($query)
	{
		require '.credentials.php';
		mysql_connect($host, $user, $passwd) or die(mysql_error());
		mysql_select_db($database)


		mysql_query($query) or die(mysql_error());
		mysql_close();
	}

}

?>