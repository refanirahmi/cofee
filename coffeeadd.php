<?php
require'coffeecontroler.php';
$coffeecontroler = new coffeecontroler();

$title = "Add a new coffee";

if(isset($_GET["update"]))
{
	$coffee = $coffeecontroler->GetCoffeeById($_GET["update"]));
	
	$content ="<form action='' method='post'>
	<fieldset>
		<legend>Add a new Coffee</legend>
		<label for='name'>Name: </label>
		<input type='text' class='inputField' name='textName' value='$coffee->name' /><br/>

		<label for='type'>Type: </label>
		<select class='inputField' name='ddlType'>
			<option value='%''>All</option>"
			.$coffeecontroler->CreateOptionValues($coffeecontroler->GetCoffeeTypes()).
		"</select><br/>

		<label for='price'>Price: </label>
		<input type='text' class='inputField' name='textPrice' value='$coffee->price' /><br/>

		<label for='roast'>Roast: </label>
		<input type='text' class='inputField' name='textRoast' value='$coffee->roast' /><br/>

		<label for='country'>Country: </label>
		<input type='text' class='inputField' name='textCountry' value='$coffee->country' /><br/>

		<label for='image'>Image: </label>
		<select class='inputField' name='dllImage'>"
		.$coffeecontroler->GetImage().
		"</select><br/>

		<label for='review'>Review: </label>
		<textarea cols='70' rows='12' name='textReview'>$coffee->review
		</textarea><br/>

		<input type='submit' value='submit'>

	</fieldset>
</form>";
}
else
{

$content ="<form action='' method='post'>
	<fieldset>
		<legend>Add a new Coffee</legend>
		<label for='name'>Name: </label>
		<input type='text' class='inputField' name='textName' /><br/>

		<label for='type'>Type: </label>
		<select class='inputField' name='ddlType'>
			<option value='%''>All</option>"
			.$coffeecontroler->CreateOptionValues($coffeecontroler->GetCoffeeTypes()).
		"</select><br/>

		<label for='price'>Price: </label>
		<input type='text' class='inputField' name='textPrice' /><br/>

		<label for='roast'>Roast: </label>
		<input type='text' class='inputField' name='textRoast' /><br/>

		<label for='country'>Country: </label>
		<input type='text' class='inputField' name='textCountry' /><br/>

		<label for='image'>Image: </label>
		<select class='inputField' name='dllImage'>"
		.$coffeecontroler->GetImage().
		"</select><br/>

		<label for='review'>Review: </label>
		<textarea cols='70' rows='12' name='textReview'>
		</textarea><br/>

		<input type='submit' value='submit'>

	</fieldset>
</form>";
}

if(isset($_GET["update"]))
{
	if(isset($_POST["textName"]))
	{
		$coffeecontroler->UpdateCoffee($_GET["update"]));
	}
}
else
{
	if(isset($_POST["textName"]))
	{
	coffeecontroler->InsertCoffee();
	}

}



include 'template.php';

?>

