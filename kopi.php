<?php

require 'coffeecontroler.php';
$coffeecontroler = new coffeecontroler();

if(isset($_POST['types']))
{
	$coffeeTables = $coffeecontroler->CreateCoffeeTables($_POST['types']);
}
else
{
	$coffeeTables = $coffeecontroler->CreateCoffeeTables('%');
}

$title = 'Coffee overview';
$content = $coffeecontroler->CreateCoffeeDropdownList(). $coffeeTables;

include 'template.php';



?>